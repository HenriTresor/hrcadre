import User from '../models/User.model.js'
import userValidObject from '../validators/userJoi.js';
import _ from 'lodash';
import createToken from '../utils/createToken.js';
import { compare } from 'bcrypt';

/**
 * @param {object}
 * @returns response object
 */
const createUser = async (req, res, next) => {
    try {

        const { value, error } = userValidObject.validate(req.body)
        if (error) {

            return next(new Error(error.details[0].message))
        }

        let { email, firstName, lastName, password } = value

        // check if user exists

        const user = await User.findOne({ email: email })
        if (user) {

            return next(new Error(`user with ${email} already exists`))
        }
        const newUser = new User({
            email,
            firstName,
            lastName,
            password
        })

        await newUser.save()

        const token = await createToken(newUser?._id)

        res.status(201).json({
            status: true,
            user: _.pick(newUser, ['email', 'firstName', 'lastName']),
            access_token: token
        })
    } catch (error) {
        console.log('error creating user', error.message);
        next(new Error('error occured creating user'))
    }
}

/**
 * @param {object}
 * @returns response object
 */

const loginUser = async (req, res, next) => {
    try {

        let { email, password } = req.body

        if (!email || !password) return next(new Error('email and password are  required'))

        let user = await User.findOne({ email: email })
        if (!user) return next(new Error('invalid email or password'))

        let comparePwd = await compare(password, user.password)

        if (!comparePwd) return next(new Error('password mismatch'))

        const token = await createToken(user?._id)
        res.status(200).json({
            status: true,
            user: _.pick(user, ['email', 'firstName', 'lastName']),
            access_token: token
        })

    } catch (error) {
        console.log('error login user', error.message)
        next(new Error('an error occured login user'))
    }
}

export const getProfile = async (req, res, next) => {

    try {

        let { userId } = req
        let user = await User.findById(userId).select('-password')

        if (!user) return next(new Error('user not found'))

        res.status(200).json({ status: true, user })
    } catch (error) {
        console.log('error getting profile', error.message)
    }
}

export { createUser, loginUser }
import { config } from "dotenv";
import dbConnect from './configs/db.config.js'
import express from 'express'
import indexRouter from './routes/index.router.js'
import userRouter from './routes/user.router.js'
import cookieParser from "cookie-parser";


config()

const app = express()
const mongo_uri = process.env.MONGO_URI
export const PORT = process.env.PORT || 4000

app.use(cookieParser())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.set('view engine', 'ejs')
app.set('views', 'views')

dbConnect(mongo_uri)
    .then(() => console.log('connection established'))
    .then(() => {
        app.listen(PORT, () => {
            console.log('listening on port ' + PORT)
        })
    })
    .catch((err) => console.log('error connecting to db', err.message))



app.use('/', indexRouter)
app.use('/users', userRouter)
const errorHandler = (err, req, res, next) => {
    res.status(500).json({ status: false, message: err.message })
}


app.all('*', (req, res) => {
    res.status(404).json({ status: false, message: 'no resource found' })
})
app.use(errorHandler)
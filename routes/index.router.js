import { Router } from "express";
import verifyToken from "../middlewares/verifyToken.js";

const router  = Router()

router.get('/', verifyToken, (req, res) => { 
    if(!req.userId) return res.redirect('/login')
    res.render('dashboard')
})

router.get('/signup', verifyToken, (req, res) => {
    if (req.userId) {
        return res.redirect('/')
    }
    res.render('signup')
})
router.get('/login', verifyToken, (req, res) => {
    if (req.userId) {
        return res.redirect('/')
    }
    res.render('login')
})

export default router
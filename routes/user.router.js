import { Router } from "express";
import { createUser, getProfile, loginUser } from "../controllers/User.controller.js";
import verifyToken from "../middlewares/verifyToken.js";

const router = Router()

router.post("/login", loginUser)
router.post("/signup", createUser)
router.get('/profile', verifyToken, getProfile)

export default router
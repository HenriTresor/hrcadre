import { verify } from "jsonwebtoken"
import { config } from "dotenv"

config()
export default async (req, res, next) => {
    try {

        let authHeader = req.headers['authorization']
        let token = authHeader ? authHeader.split(' ')[1] : req.cookies?.access_token

        const id = await verify(authHeader ? token.split('=')[1] : token, process.env.ACCESS_SECRET_TOKEN)
        req.userId = id.id
        next()
    } catch (error) {
        console.log(error.message);
        next()
    }
} 
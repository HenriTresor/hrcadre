## User management project - HRCADRE consultation

### Made with Nodejs and Mongodb

### To start

### git clone https://gitlab.com/HenriTresor/hrcadre.git

### yarn install or npm install

### create .env file add

 <ul>
    <li>ACCESS_SECRET_TOKEN={your token}</li>
    <li>MONGO_URI={your mongodb url}</li>
 </ul>

### run yarn dev or npm dev

### visit site on http://localhost:4000

### Made with 💕 by HenriTresor

import mongoose from "mongoose";


export default async (uri) =>{
    try {
        return await mongoose.connect(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
    } catch (error) {
        console.log('error connecting to db', error.message);
    }
}
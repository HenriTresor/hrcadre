import { hash } from 'bcrypt'
import { Schema, model } from 'mongoose'

const UserSchema = new Schema({
    email: { type: String, required: true, unique: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    password: { type: String, required: true },
})

UserSchema.pre('save', async function () {
    try {

        const hashedPwd = await hash(this.password, 10)
        this.password = hashedPwd

    } catch (error) {
        console.log('error hashing password', error.message)
    }
})

export default model('users', UserSchema)
import jwt from 'jsonwebtoken'
import { config } from 'dotenv'

config()
export default async (id) => {
    return jwt.sign({id: id}, process.env.ACCESS_SECRET_TOKEN)
}
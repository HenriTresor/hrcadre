import Joi from 'joi'

const userValidObject = Joi.object({
    email: Joi.string().email().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    password: Joi.string().min(5).required()
})

export default userValidObject